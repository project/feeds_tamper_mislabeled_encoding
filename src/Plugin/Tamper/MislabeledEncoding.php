<?php

namespace Drupal\feeds_tamper_mislabeled_encoding\Plugin\Tamper;

use Drupal\tamper\Exception\TamperException;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

/**
 * Plugin implementation for mislabeled_encoding.
 *
 * @Tamper(
 *   id = "mislabeled_encoding",
 *   label = @Translation("Correct Mislabeled Encoding"),
 *   description = @Translation("Fix Windows-1252 chars (80-9f) in ISO 8859-1 labeled string with UTF-8 chars."),
 *   category = "Text"
 * )
 */
class MislabeledEncoding extends TamperBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, ?TamperableItemInterface $item = NULL) {
    if (!is_string($data)) {
      throw new TamperException('Input should be a string.');
    }
    $char_replace_table = [
      // Euro Sign.
      "\xc2\x80" => "\u{20ac}",

      // Single Low-9 Quotation Mark.
      "\xc2\x82" => "\u{201a}",

      // Latin Small Letter F with Hook.
      "\xc2\x83" => "\u{0192}",

      // Double Low-9 Quotation Mark.
      "\xc2\x84" => "\u{201e}",

      // Horizontal Ellipsis.
      "\xc2\x85" => "\u{2026}",

      // Dagger.
      "\xc2\x86" => "\u{2020}",

      // Double Dagger.
      "\xc2\x87" => "\u{2021}",

      // Modifier Letter Circumflex Accent.
      "\xc2\x88" => "\u{02c6}",

      // Per Mille Sign.
      "\xc2\x89" => "\u{2030}",

      // Latin Capital Letter S with Caron.
      "\xc2\x8a" => "\u{0160}",

      // Single Left-Pointing Angle Quotation Mark.
      "\xc2\x8b" => "\u{2039}",

      // Latin Capital Ligature Oe.
      "\xc2\x8c" => "\u{0152}",

      // Latin Capital Letter Z with Caron.
      "\xc2\x8e" => "\u{017d}",

      // Left Single Quotation Mark.
      "\xc2\x91" => "\u{2018}",

      // Right Single Quotation Mark.
      "\xc2\x92" => "\u{2019}",

      // Left Double Quotation Mark.
      "\xc2\x93" => "\u{201c}",

      // Right Double Quotation Mark.
      "\xc2\x94" => "\u{201d}",

      // Bullet.
      "\xc2\x95" => "\u{2022}",

      // En Dash.
      "\xc2\x96" => "\u{2013}",

      // Em Dash.
      "\xc2\x97" => "\u{2014}",

      // Small Tilde.
      "\xc2\x98" => "\u{02dc}",

      // Trade Mark Sign.
      "\xc2\x99" => "\u{2122}",

      // Latin Small Letter S with Caron.
      "\xc2\x9a" => "\u{0161}",

      // Single Right-Pointing Angle Quotation Mark.
      "\xc2\x9b" => "\u{203a}",

      // Latin Small Ligature Oe.
      "\xc2\x9c" => "\u{0153}",

      // Latin Small Letter Z with Caron.
      "\xc2\x9e" => "\u{017e}",

      // Latin Capital Letter Y with Diaeresis.
      "\xc2\x9f" => "\u{0178}",
    ];
    $find = array_keys($char_replace_table);
    $data = str_replace($find, $char_replace_table, $data);
    return $data;
  }

}
